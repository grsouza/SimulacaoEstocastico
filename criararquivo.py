import random

N = 5000

with open("entrada.txt", "w+") as file:
    for i in range(N):
        file.write(str(i) + " " + str(random.randint(1, 35)*5000))
        if i < N-1:
            file.write("\n")
