#include <fstream>
#include <iostream>
#include <map>
#include <queue>
#include <unistd.h>
#include <vector>

/*
 * Quantas vezes os processos voltaram pra fila;
 * Tempo total de execucao;
 * Tempo total requerido pelos processos;
 */

enum ProcessState { READY, RUNNING, CANCELED, FINISHED, INTERRUPTED };

using namespace std;

static const unsigned TIME_UNIT = 1000;

unsigned long ptotal = 0;
unsigned long totalTick = 0;

struct Process {
  unsigned id = 0;
  unsigned long totalTime = 0;
  long remainingTime = 0;
  unsigned returnedToQueue = 0;
  ProcessState state = READY;
};

class CPU {
  unsigned long totalExecution = 0;
  unsigned id = 0;
  Process p;
  unsigned long elapsedTime = 0;
  unsigned long average = 0;

public:
  CPU() {
    static int id = 0;
    this->id = id++;
  }
  unsigned get_id(void) { return id; }
  ProcessState tick(unsigned tu);
  void assign_process(Process);
  Process get_process(void) { return p; }
  bool can_assign(void) { return this->p.state != RUNNING; }
  unsigned long get_total() { return totalExecution; }
};

ProcessState CPU::tick(unsigned timeUnit) {
  // usleep(500);

  ProcessState state;

  if (p.state == FINISHED)
    return READY;

  totalExecution += timeUnit;
  elapsedTime += timeUnit;
  p.remainingTime -= timeUnit;

  if (p.remainingTime <= 0) {
    state = FINISHED;

#if VERBOSE
    cout << "P[" << p.id << "] finished." << endl;
#endif

  } else if (elapsedTime > average) {
    ++p.returnedToQueue;

#if VERBOSE
    cout << "P[" << p.id << "] returned to queue." << endl;
#endif

    if (p.returnedToQueue >= 3) {
      state = CANCELED;
#if VERBOSE
      cout << "P[" << p.id << "] canceled." << endl;
#endif
    } else {
      state = INTERRUPTED;
#if VERBOSE
      cout << "P[" << p.id << "] interrupted." << endl;
#endif
    }
  } else {
    state = RUNNING;
  }
  p.state = state;
  return state;
}

void CPU::assign_process(Process p) {
  if (can_assign()) {
    // if this->p.totalTime == 0, then p is the first process being assigend,
    // and it's total time should be used as average.
    average = (this->p.state == READY) ? p.totalTime
                                       : (this->p.totalTime + p.totalTime) / 2;
    this->p = p;
    this->p.state = RUNNING;
    elapsedTime = 0;
  }
}

class Simulation {
  vector<CPU> cpus;
  vector<queue<Process>> pqueue;
  queue<Process> fqueue;
  bool multiqueue;

public:
  Simulation(queue<Process>, unsigned cpus, bool multiqueue);
  void run(void);
  void dump(ofstream &);
};

Simulation::Simulation(queue<Process> queue, unsigned ncpus, bool multiqueue)
    : multiqueue(multiqueue) {
  unsigned nqueues = multiqueue ? ncpus : 1;
  unsigned long len = queue.size();

  cpus.resize(ncpus);
  pqueue.resize(nqueues);

  for (int i = 0; i < nqueues; ++i) {
    for (int j = 0; j < len / ncpus; ++j) {
      pqueue[i].push(queue.front());
      queue.pop();
    }
  }
  while (queue.size() > 0) {
    pqueue[0].push(queue.front());
    queue.pop();
  }
}

void Simulation::run(void) {
  int idx;
  bool keep_simulating;

  do {
    keep_simulating = false;

    for (auto &cpu : cpus) {
      idx = multiqueue ? cpu.get_id() : 0;
      if (cpu.can_assign() && !pqueue[idx].empty()) {
        cpu.assign_process(pqueue[idx].front());
        pqueue[idx].pop();

#if VERBOSE
        cout << "P[" << cpu.get_process().id << "] assigned to CPU ["
             << cpu.get_id() << "]." << endl;
#endif
      }
    }

    for (auto &cpu : cpus) {
      idx = multiqueue ? cpu.get_id() : 0;

      ProcessState state = cpu.tick(TIME_UNIT);

      switch (state) {
      case FINISHED:
      case CANCELED:
        fqueue.push(cpu.get_process());
        break;

      case INTERRUPTED:
        pqueue[idx].push(cpu.get_process());
        break;

      case READY:
      case RUNNING:
        break;
      }
    }

    for (auto cpu : cpus)
      keep_simulating = keep_simulating || !cpu.can_assign();

    for (auto queue : pqueue)
      keep_simulating = keep_simulating || !queue.empty();

    totalTick++;
  } while (keep_simulating);
}

void Simulation::dump(ofstream &ofs) {
  map<int, int> returns;
  int totalReturnsToQueue = 0;
  int canceled = 0;
  Process p;

  while (!fqueue.empty()) {
    p = fqueue.front();
    fqueue.pop();

    if (returns.find(p.returnedToQueue) != returns.end()) {
      ++returns[p.returnedToQueue];
    } else {
      returns[p.returnedToQueue] = 1;
    }

    totalReturnsToQueue += p.returnedToQueue;
    if (p.returnedToQueue >= 3)
      ++canceled;
  }

  ofs << "totalReturnsToQueue = " << totalReturnsToQueue << endl
      << "canceled = " << canceled << endl;

  ofs << endl << "number of returns" << endl;
  for (const auto &p : returns) {
    if (p.first == 0)
      continue;
    ofs << p.first << " : " << p.second << endl;
  }

  ofs << endl;

  for (int i = 0; i < cpus.size(); ++i)
    ofs << "CPU " << i << " : " << cpus[i].get_total() << endl;

  ofs << endl
      << "ptotal = " << ptotal << endl
      << "tick = " << totalTick << endl;
}

int main(int argc, char **argv) {
  queue<Process> pqueue;
  ifstream ifs;
  ofstream ofs;
  Process p;

  ifs.open("entrada.txt", ifstream::in);
  while (!ifs.eof()) {
    ifs >> p.id >> p.totalTime;
    p.remainingTime = p.totalTime;
    ptotal += p.remainingTime;
    pqueue.push(p);
  }
  ifs.close();

  if (argc < 3) {
    cout << "Usage: " << argv[0]
         << "[numero de processadores] [fila multipla(0 ou 1)]" << endl;
    abort();
  }

  Simulation sim(pqueue, atoi(argv[1]), atoi(argv[2]));
  sim.run();

  ofs.open(argc == 4 ? argv[3] : "saida.txt", ofstream::out);
  sim.dump(ofs);
  ofs.close();
}
